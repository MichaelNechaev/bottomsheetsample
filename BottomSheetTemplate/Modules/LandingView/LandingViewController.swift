//
//  LandingViewController.swift
//  BottomSheetTemplate
//
//  Created by Michael Nechaev on 17/01/2019.
//  Copyright © 2019 Michael Nechaev. All rights reserved.
//

import UIKit

final class LandingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let staticDataArray = ["ЛЮБАЯ", "ВЬЮХА", "ТАБЛИЦА", "НА VC1", "МОЖНО", "ЮЗАТЬ", "ЕГО", "ИЛИ"]
    var bottomSheetVC: BottomSheetViewController?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addBottomSheetView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staticDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "landingCell", for: indexPath) as! LandingTableViewCell
        cell.nameLabel.text = staticDataArray[indexPath.row]
        cell.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        return cell
    }
    
    func addBottomSheetView() {
        if self.bottomSheetVC == nil {
            let bottomSheetVC = BottomSheetViewController()
            self.bottomSheetVC = bottomSheetVC
            
            self.addChild(bottomSheetVC)
            self.view.addSubview(bottomSheetVC.view)
            bottomSheetVC.didMove(toParent: self)
            
            let height = view.frame.height
            let width  = view.frame.width
            bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        }
    }
    
}
