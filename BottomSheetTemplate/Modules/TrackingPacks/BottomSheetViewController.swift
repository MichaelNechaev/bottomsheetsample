//
//  TrackingPacksViewController.swift
//  BottomSheetTemplate
//
//  Created by Michael Nechaev on 18/01/2019.
//  Copyright © 2019 Michael Nechaev. All rights reserved.
//

import UIKit

final class BottomSheetViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - IBOutlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    // MARK: - Private properties

    private let items = ["ЮЗАТЬ","ЭТОТ","КОТНРОЛЛЕР","ЕЩЕ","ТАЩИТЬ","ПРИКОЛЬНО","ЕГО"]
    private let fullViewHeight: CGFloat = 100
    private var isExpanded = false
    
    private var partialViewHeight: CGFloat {
        let viewPartHeight: CGFloat = UIDevice().isLargerThanIPhoneX ? 231 : 173
        return UIScreen.main.bounds.height - viewPartHeight
    }
    
    private var usersFrameWidth: CGFloat? {
        get {
            let width = UserDefaults.standard.value(forKey: "bottomSheetFrameWidth") as? CGFloat
            return width
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "bottomSheetFrameWidth")
        }
    }
    
    private var usersFrameHeight: CGFloat? {
        get {
            let height = UserDefaults.standard.value(forKey: "bottomSheetFrameHeight") as? CGFloat
            return height
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "bottomSheetFrameHeight")
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupGesture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        usersFrameWidth = usersFrameWidth ?? self.view.frame.width
        usersFrameHeight = usersFrameHeight ?? self.view.frame.height
        
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frameWidth = self?.usersFrameWidth ?? 0
            let frameHeight = self?.usersFrameHeight ?? 0
            let yComponent = self?.partialViewHeight
            self?.view.frame = CGRect(x: 0, y: yComponent ?? 0, width: frameWidth, height: frameHeight - 100)
            self?.view.layoutIfNeeded()
        })
        self.headerView.roundedTopOnView()
        
        //load items here
        
        self.tableView.isHidden = !(self.items.count > 0)
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        self.view.layoutIfNeeded()
    }
    
    func showPackagesView() {
        self.tableView.isScrollEnabled = false
        let duration = 0.63
        UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] in
            self?.view.frame = CGRect(x: 0, y: self?.fullViewHeight ?? 0, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
            self?.isExpanded = true
            }, completion: { [weak self] _ in
                self?.tableView.isScrollEnabled = true
        })
    }
    
    func hidePackagesView() {
        self.tableView.isScrollEnabled = false
        let duration = 0.63
        UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] in
            self?.view.frame = CGRect(x: 0, y: self?.partialViewHeight ?? 0, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
            self?.isExpanded = false
            }, completion: { [weak self] _ in
                self?.tableView.isScrollEnabled = true
        })
    }
    
    // MARK: -Gesture
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        let y = view.frame.minY
        if (y == fullViewHeight && tableView.contentOffset.y == 0 && direction > 0) || (y == partialViewHeight) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
        return false
    }
    
}

// MARK: - IBActions

extension BottomSheetViewController {
    
    @IBAction func headerButtonTapped(_ sender: Any) {
        if self.isExpanded {
            self.hidePackagesView()
        } else {
            self.showPackagesView()
        }
    }
    
    @objc
    func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        let y = self.view.frame.minY
        if (y + translation.y >= fullViewHeight) && (y + translation.y <= partialViewHeight) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullViewHeight) / -velocity.y) : Double((partialViewHeight - y) / velocity.y )
            duration = duration > 1.3 ? 1 : duration
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] in
                if  velocity.y >= 0 {
                    self?.view.frame = CGRect(x: 0, y: self?.partialViewHeight ?? 0, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
                    self?.isExpanded = false
                } else {
                    self?.view.frame = CGRect(x: 0, y: self?.fullViewHeight ?? 0, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
                    self?.isExpanded = true
                }
                
                }, completion: { [weak self] _ in
                    if ( velocity.y < 0 ) {
                        self?.tableView.isScrollEnabled = true
                    }
            })
        }
    }

}

// MARK: - Setup

private extension BottomSheetViewController {
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DefaultTableViewCell", bundle: nil), forCellReuseIdentifier: "defCell")
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.headerView.backgroundColor = .lightGray
    }
    
    func setupGesture() {
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGesture(_:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
}

// MARK: - UITableViewDelegate

extension BottomSheetViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        //        if let delegate = self.delegate {
        //            delegate.trackPackage(with: self.packages?[indexPath.row].trackingNumber)
        //            self.hidePackagesView()
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

}

// MARK: - UITableViewDataSource

extension BottomSheetViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "defCell", for: indexPath) as! DefaultTableViewCell
        let item = self.items[indexPath.row]
        cell.nameLabel.text = item
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return cell
    }
    
}
