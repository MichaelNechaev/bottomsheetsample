//
//  ViewController.swift
//  BottomSheetTemplate
//
//  Created by Michael Nechaev on 17/01/2019.
//  Copyright © 2019 Michael Nechaev. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {

    @IBAction func openAction(_ sender: Any) {
        performSegue(withIdentifier: "landing", sender: nil)
    }

}

