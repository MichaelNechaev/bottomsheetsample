//
//  UIView.swift
//  BottomSheetTemplate
//
//  Created by Михаил Нечаев on 30/08/2019.
//  Copyright © 2019 Michael Nechaev. All rights reserved.
//

import UIKit

extension UIView {
    
    func roundedTopOnView() {
        let maskPath1 = UIBezierPath(roundedRect: superview?.bounds ?? bounds,
                                     byRoundingCorners: [.topLeft, .topRight],
                                     cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = superview?.bounds ?? bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }

}
