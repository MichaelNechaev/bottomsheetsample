//
//  UIDevice.swift
//  BottomSheetTemplate
//
//  Created by Михаил Нечаев on 03/09/2019.
//  Copyright © 2019 Michael Nechaev. All rights reserved.
//

import UIKit

extension UIDevice {
    
    var isLargerThanIPhoneX: Bool {
        return UIScreen.main.nativeBounds.height >= 2436 || UIScreen.main.nativeBounds.height == 1792
    }
    
}
